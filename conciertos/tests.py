from django.test import TestCase
from . import views
import urllib.request

# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        """Ejecutando antes de los test"""

    def test_index_status(self):
        """Test de la función index, estado"""
        response = self.client.get('/conciertos/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        """Test de la función index, función"""
        response = self.client.get('/conciertos/')
        self.assertEqual(response.resolver_match.func, views.index)
